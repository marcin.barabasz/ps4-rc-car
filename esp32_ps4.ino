#include <PS4Controller.h>
#include <ESP32Servo.h> 
#include "esp_bt_main.h"
#include "esp_bt_device.h"
#include"esp_gap_bt_api.h"
#include "esp_err.h"
#include <EEPROM.h>

Servo esc; 
Servo steering;

#define ESC_PIN 12
#define STEERING_PIN 13
#define STEERING_FLIP 1
#define REMOVE_BONDED_DEVICES 1   // <- Set to 0 to view all bonded devices addresses, set to 1 to remove
#define PAIR_MAX_DEVICES 20
#define EEPROM_SIZE 1

uint8_t pairedDeviceBtAddr[PAIR_MAX_DEVICES][6];
char bda_str[18];
int escVal;
int steeringVal;
int removeStoredDevices = 0;

void setup() {
  Serial.begin(115200);
  // initialize EEPROM with predefined size
  EEPROM.begin(EEPROM_SIZE);
  removeStoredDevices = EEPROM.read(0);
  
  // basically every other restart it will remove the stored devices
  if (removeStoredDevices == 1) {
    Serial.println("RemovingStoredDevices");
    removePreviouslyPairedDevices();
    EEPROM.write(0, 0);
    EEPROM.commit();
    ESP.restart();
  } else {
    Serial.println("Skipping removal of stored devices");
    EEPROM.write(0, 1);
    EEPROM.commit();
  }
  
  
  PS4.begin("5c:17:cf:2d:0e:09");
  Serial.println("Ready.");

  // Allow allocation of all timers
  ESP32PWM::allocateTimer(0);
  ESP32PWM::allocateTimer(1);
  ESP32PWM::allocateTimer(2);
  ESP32PWM::allocateTimer(3);
  esc.setPeriodHertz(50);// Standard 50hz servo
  
  // attaches the servo on pin 18 to the servo object
  // using SG90 servo min/max of 500us and 2400us
  // for MG995 large servo, use 1000us and 2000us,
  esc.attach(ESC_PIN, 500, 2400);   
  steering.setPeriodHertz(50);// Standard 50hz servo
  steering.attach(STEERING_PIN, 500, 2400);
}

void loop() {
  // Below has all accessible outputs from the controller
  if (PS4.isConnected()) {
    escVal = 100;
    if (PS4.R2Value()) {
      escVal = map(PS4.R2Value(), 0, 255, 100, 120);
    }

    if (PS4.L2Value()) {
      escVal = 100 - map(PS4.L2Value(), 0, 255, 0, 10);
    }

    // 90 is the middle 0 is total left 180 is total right, 45-135 is somewhat in the middle
    if (STEERING_FLIP == 1){
      steeringVal = 180 - map(PS4.LStickX(), -128, 128, 45, 135); 
    }else {
      steeringVal = map(PS4.LStickX(), -128, 128, 45, 135); 
    }
    
    Serial.printf("Left Stick :%d R2: %d L2: %d, ESC:%d, STEERING:%d\n", PS4.LStickX(), PS4.R2Value(), PS4.L2Value(), escVal, steeringVal);
    esc.write(escVal);
    steering.write(steeringVal);
  }
}


void removePreviouslyPairedDevices() {
  initBluetooth();
  Serial.print("ESP32 bluetooth address: "); Serial.println(bda2str(esp_bt_dev_get_address(), bda_str, 18));
  // Get the numbers of bonded/paired devices in the BT module
  int count = esp_bt_gap_get_bond_device_num();
  if(!count) {
    Serial.println("No bonded device found.");
  } else {
    Serial.print("Bonded device count: "); Serial.println(count);
    if(PAIR_MAX_DEVICES < count) {
      count = PAIR_MAX_DEVICES; 
      Serial.print("Reset bonded device count: "); Serial.println(count);
    }
    esp_err_t tError =  esp_bt_gap_get_bond_device_list(&count, pairedDeviceBtAddr);
    if(ESP_OK == tError) {
      for(int i = 0; i < count; i++) {
        Serial.print("Found bonded device # "); Serial.print(i); Serial.print(" -> ");
        Serial.println(bda2str(pairedDeviceBtAddr[i], bda_str, 18));     
        if(REMOVE_BONDED_DEVICES) {
          esp_err_t tError = esp_bt_gap_remove_bond_device(pairedDeviceBtAddr[i]);
          if(ESP_OK == tError) {
            Serial.print("Removed bonded device # "); 
          } else {
            Serial.print("Failed to remove bonded device # ");
          }
          Serial.println(i);
        }
      }        
    }
  }
}

bool initBluetooth()
{
  if(!btStart()) {
    Serial.println("Failed to initialize controller");
    return false;
  }
 
  if(esp_bluedroid_init() != ESP_OK) {
    Serial.println("Failed to initialize bluedroid");
    return false;
  }
 
  if(esp_bluedroid_enable() != ESP_OK) {
    Serial.println("Failed to enable bluedroid");
    return false;
  }
  return true;
}

char *bda2str(const uint8_t* bda, char *str, size_t size)
{
  if (bda == NULL || str == NULL || size < 18) {
    return NULL;
  }
  sprintf(str, "%02x:%02x:%02x:%02x:%02x:%02x",
          bda[0], bda[1], bda[2], bda[3], bda[4], bda[5]);
  return str;
}
